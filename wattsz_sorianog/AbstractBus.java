package wattsz_sorianog;

import java.util.ArrayList;
import java.util.List;
/**
 * Lab Week 5
 *
 * @author wattsz, sorianog
 * @version Course: SE 2811 011
 *          <p> Quarter: Winter 2014-15
 * @created 1/16/2015 8:17 PM
 */
public abstract class AbstractBus {

	private List<BusObserver> observers = new ArrayList<BusObserver>();
	
	private double velocity;
	
	private VehicleText vehicle;
	
	public void addObserver(BusObserver observer){
		observers.add(observer);
	};
	
	public abstract void notifyObservers();		
	
	
	public void removeObserver(BusObserver observer){
		
		observers.remove(observer);
		
	};
		
		
		
		
	}
	
	
	
	
	

