package wattsz_sorianog;

import java.time.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Lab Week 5
 *
 * @author sorianog
 * @version Course: SE 2811 011
 *          <p> Quarter: Winter 2014-15
 * @created 1/19/2015 4:58 PM
 *
 * A Bus class with additional functionality
 */
public class Bus extends AbstractBus {
	/**
	 * Amount of miles per longitude degree
	 */
	private static final double LON_SCALAR = 50.63;
	/**
	 * Amount of miles per latitude degree
	 */
	private static final double LAT_SCALAR = 69.03;


	private static volatile Map<String, Bus> buses = new HashMap<String, Bus>();
	
	private static volatile Bus bus = null;
	/**
	 * ID of the bus
	 */
	private String busID;
	/**
	 * Timestamp(date and time) of the bus
	 */
	private String tmstmp;
	/**
	 * Bus longitude (degrees)
	 */
	private double lon;
	/**
	 * Bus latitude (degrees)
	 */
	private double lat;

	/**
	 * List of observers
	 */
	private List<BusObserver> observers;

	private long prevTime = -1;

	private long currTime;

	private double prevLon = -1;

	private double prevLat = -1;




	//private VehicleText vt;

	/**
	 * Bus constructor - initializes variables
	 * @param vehicleText raw text data for a bus.
	 */
	private Bus(VehicleText vehicleText){
		this.addObserver(new GraphicalObserver());
		this.addObserver(new TextualObserver());
		updateBus(vehicleText);
		buses.put(busID, this);
	}

	/**
	 * Bus ID getter
	 * @return busID
	 */
	public String getID(){
		return busID;
	}

	/**
	 * Timestamp getter
	 * @return tmstmp
	 */
	public String getTime(){
		return tmstmp;
	}

	/**
	 * Longitude getter
	 * @return lon
	 */
	public double getLon(){
		return lon;
	}

	/**
	 * Latitude getter
	 * @return lat
	 */
	public double getLat(){
		return lat;
	}

	/**
	 * Adds the observer to the observer list
	 * @param observer a type of BusObserver
	 */
	public void addObserver(BusObserver observer){
		if(observers == null){
			observers = new ArrayList<BusObserver>();
		}
		observers.add(observer);

	}

	private double calcVelocity(){
		double velocity = 0;
		

		if(prevTime != -1 && prevLat != -1 && prevLon != -1){

			double milesEast = (lon) * LON_SCALAR;
			double milesNorth = (lat) * LAT_SCALAR;
			double prevmilesEast = (prevLon) * LON_SCALAR;
			double prevmilesNorth = (prevLat) * LAT_SCALAR;	

			double distance = (Math.sqrt(((prevmilesNorth-milesNorth)*(prevmilesNorth-milesNorth))+((prevmilesEast-milesEast)*(prevmilesEast-milesEast)))); 
			if( (currTime-prevTime) != 0){
				velocity = distance/ (currTime-prevTime);
			}
		}

		prevLat = lat;
		prevLon = lon;
		prevTime = currTime;
		return velocity/3600;

	}
	/**
	 * Creates a singleton bus based on the VID
	 * @param vt the VehicleText that will be passed to the constructor
	 * @return returns a Bus 
	 */
	public static Bus createBus(VehicleText vt){
		if (!buses.containsKey(vt.vid)){
			synchronized (Bus.class){
				if (!buses.containsKey(vt.vid)){
					bus = new Bus(vt);
				}
			}
		} else {
			bus = buses.get(vt.vid);
		}
		return bus;
	}

	
	/**
	 * notifies all the observers
	 */
	@Override
	public void notifyObservers() {

		double velocity = calcVelocity();
		for(BusObserver observer: observers){

			observer.update(lon, lat, velocity);
		}
	}

	/**
	 * String representation of Bus object
	 * @return textual information about the bus
	 */
	public String toString(){
		return "[Date & Time]: " + tmstmp + "\t [Bus ID]: " + busID + "\n [Longitude]: " + lon + "\n [Latitude]: " + lat;
	}
	
	/**
	 * Updates the proper bus with new info
	 * @param vt the VehicleText with the updated data
	 */
	public void updateBus(VehicleText vt){
		
		currTime =  LocalDateTime.now(ZoneId.of("America/Chicago")).toEpochSecond((ZoneOffset.of("-06:00")));
		busID = vt.vid;
		tmstmp = vt.tmstmp;
		lon = Double.parseDouble(vt.lon);
		lat = Double.parseDouble(vt.lat);
		
	}

}
