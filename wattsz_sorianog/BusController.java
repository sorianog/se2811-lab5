package wattsz_sorianog;

import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.List;

/**
 * Lab Week 5
 *
 * @author sorianog
 * @version Course: SE 2811 011
 *          <p> Quarter: Winter 2014-15
 * @created 1/16/2015 8:38 PM
 *
 * The main controller for the application
 */
public class BusController {

    /**
     * A wrapper to access MCTS data
     */
    private static RealtimeWrapper fetcher;
    /**
     * A list of Bus objects
     */
    private static List<Bus> buses = new ArrayList<Bus>();

    /**
     * BusController constructor - creates a new RealtimeWrapper to allow access to MCTS data
     */
    public BusController(){
        try {
            fetcher = new RealtimeWrapper("keyFile.txt");
        } catch (RealtimeWrapperException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    /**
     * Fetches available bus data
     * @return buses
     */
    public List<Bus> fetchBusData(){
        try {
            List<VehicleText> busesText = fetcher.fetchVehicles();
            for(VehicleText v : busesText){
            	Bus tempBus = Bus.createBus(v);
            	if(!(buses.contains(tempBus))){
            		buses.add(tempBus);
            	
            	}
            	
            	
            }
            for(VehicleText v : busesText){
            	for(Bus b : buses){
            		if(b.getID().equals(v.vid)){
            			
            			b.updateBus(v);
            		}
            		
            	}
            	
            	
            }
        } catch (RealtimeWrapperException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return buses;
    }

    /**
     * Notifies observers track a bus.
     * @param id of a bus
     */
    public void trackBus(String id){
        for (Bus bus : buses){
            if (id.equals(bus.getID())){
               
            	bus.notifyObservers();
            	
            }
        }
    }

   
}
