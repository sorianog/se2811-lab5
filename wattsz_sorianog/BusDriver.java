
package wattsz_sorianog;
/**
 * Lab Week 5
 *
 * @author wattsz, sorianog
 * @version Course: SE 2811 011
 *          <p> Quarter: Winter 2014-15
 * @created 1/16/2015 8:17 PM
 */
public class BusDriver {
	
	/**
	 * Creates a new UI 
	 * @param args - ignored
	 */
	public static void main(String[] args) {
		new UI();
	}

}
