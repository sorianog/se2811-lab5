package wattsz_sorianog;

/**
 * Lab Week 5
 *
 * @author wattsz, sorianog
 * @version Course: SE 2811 011
 *          <p> Quarter: Winter 2014-15
 * @created 1/15/2015 10:49 AM
 *
 * A generic BusObserver interface
 */
public interface BusObserver {

    /**
     * Provides the new position and velocity of the bus
     * @param lon the longitude of the bus
     * @param lat the latitude of the bus
     * @param velocity the velocity of the bus
     */
    public void update(double lon, double lat, double velocity);

}
