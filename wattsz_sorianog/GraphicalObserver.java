package wattsz_sorianog;

import java.awt.Color;

import edu.msoe.se1010.winPlotter.WinPlotter;

public class GraphicalObserver implements BusObserver {

    /**
     * MKE City Hall longitude (degrees)
     */
	private static final double HALL_POS_EAST = -87.90976;
    /**
     * MKE City Hall latitude (degrees)
     */
	private static final double HALL_POS_NORTH = 43.04173;
    /**
     * Amount of miles per longitude degree
     */
	private static final double LON_SCALAR = 50.6300;
    /**
     * Amount of miles per latitude degree
     */
	private static final double LAT_SCALAR = 69.0300;
	/**
	 * Plotter used to display bus data 
	 */
	private WinPlotter plotter;

	public GraphicalObserver(){


	}
	/**
	 * Takes in the lon, lat, and plots them.
	 */
	@Override
	public void update(double lon, double lat, double velocity){




		plotter = new WinPlotter();

		plotter.erase();
		plotter.setWindowSize(1000, 1000);
		plotter.setPlotBoundaries(-(Math.abs(HALL_POS_EAST/LON_SCALAR)+(10)), -(Math.abs(HALL_POS_NORTH/LAT_SCALAR)+((10))),
				(Math.abs(HALL_POS_EAST/LON_SCALAR)+(10)), (Math.abs(HALL_POS_NORTH/LAT_SCALAR)+(10)));
		plotter.setGrid(true, 1.0, 1.0,  Color.blue);		
		plotter.setPenColor(255, 0, 0);
		plotter.moveTo(0, 0);
		plotter.drawTo((lon - HALL_POS_EAST) * LON_SCALAR,(lat - HALL_POS_NORTH) * LAT_SCALAR);
		plotter.drawPoint((lon - HALL_POS_EAST) * LON_SCALAR,(lat - HALL_POS_NORTH) * LAT_SCALAR);
		plotter.printAt((lon - HALL_POS_EAST) * LON_SCALAR,(lat - HALL_POS_NORTH) * LAT_SCALAR, "Bus");
		plotter.drawPoint(0, 0);
		plotter.printAt(0, 0, "Town Hall");

	}


}
