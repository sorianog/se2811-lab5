package wattsz_sorianog;

import javax.swing.JOptionPane;
import java.text.DecimalFormat;

/**
 * Lab Week 5
 *
 * @author sorianog
 * @version Course: SE 2811 011
 *          <p> Quarter: Winter 2014-15
 * @created 1/19/2015 6:37 PM
 *
 * An observer to update textual data
 */
public class TextualObserver implements BusObserver {

    /**
     * MKE City Hall longitude (degrees)
     */
    private static final double HALL_POS_EAST = -87.90976;
    /**
     * MKE City Hall latitude (degrees)
     */
    private static final double HALL_POS_NORTH = 43.04173;
    /**
     * Amount of miles per longitude degree
     */
    private static final double LON_SCALAR = 50.63;
    /**
     * Amount of miles per latitude degree
     */
    private static final double LAT_SCALAR = 69.03;

    /**
     * empty constructor
     */
    public TextualObserver(){

    }

    /**
     * Determines the position of the bus in miles.
     * Creates a pop-up displaying the position of a bus in respect to MKE City Hall and it's current velocity.
     * @param lon the longitude of the bus
     * @param lat the latitude of the bus
     * @param velocity the velocity of the bus
     */
    @Override
    public void update(double lon, double lat, double velocity) {
        DecimalFormat df = new DecimalFormat("##.#####");
        double milesEast = (lon - HALL_POS_EAST) * LON_SCALAR;
        double milesNorth = (lat - HALL_POS_NORTH) * LAT_SCALAR;
        JOptionPane.showMessageDialog(null, "The currently selected bus is located " + df.format(milesEast) + " miles east and " +
                                      df.format(milesNorth) + " miles north of Milwaukee City Hall.\nIts velocity is " + df.format(velocity) + " mph.");
    }
}
