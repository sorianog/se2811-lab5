package wattsz_sorianog;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;


/**
 * Lab Week 5
 *
 * @author wattsz
 * @version Course: SE 2811 011
 *          <p> Quarter: Winter 2014-15
 * @created 1/16/2015 8:17 PM
 */

public class UI extends JFrame{
	/**
	 * the JList that displays the buses
	 */
	private JList<Bus> buses;
	/**
	 * the controller that talks to the entities
	 */
	private BusController controller = new BusController();
	/**
	 * The array that is held in the JList
	 */
	private Bus jListObjects[] = new Bus[0];
	/**
	 * Right side of the UI
	 */
	private JPanel rightPanel = new JPanel();
	/**
	 * Left side fo the UI
	 */
	private JPanel leftPanel = new JPanel();
	/**
	 * Button for fetching
	 */
	private JButton fetch = new JButton("Fetch Bus List");
	/**
	 * Button for tracking
	 */
	private JButton track = new JButton("Track Selected Bus");
	
	/**
	 * Constructor for the UI
	 * Creates and adds all of the components
	 */
	public UI(){
		leftPanel.setLayout(new GridLayout(2, 1));	

		leftPanel.add(fetch);
		leftPanel.add(track);
		

		this.setLayout(new GridLayout(1,2));

		this.add(leftPanel);
		this.add(rightPanel);


		this.pack();
		this.setSize(1000, 1000);
		rightPanel.setSize(500,500);
		leftPanel.setSize(500,500);

		addActionListeners();
		this.setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

	}
	/**
	 * Updates the contents of the JList
	 * @param list List of Buses to display
	 */
	public void updateJList(List<Bus> list){
		jListObjects = new Bus[list.size()];

		if(buses == null){
			buses = new JList<Bus>(jListObjects);
			this.rightPanel.add(buses);
			buses.setFixedCellWidth((this.getWidth()/2));
			buses.setFixedCellHeight((this.getHeight()/list.size())); //resizes the boxes to fit
			buses.updateUI();

		}
		int temp = 0;
		for(Bus v : list){					
			jListObjects[temp] = v;				// JList needs an array...
			temp++;
			System.out.println("current bus: "+v);
		}

	}
	/**
	 * adds all of the ActionListeners
	 */
	public void addActionListeners(){

		fetch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateJList(controller.fetchBusData());
				buses.updateUI();

			}
		});

		track.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.trackBus(buses.getSelectedValue().getID());

			}
		});
		/*text.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		graph.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});*/
	}

}
